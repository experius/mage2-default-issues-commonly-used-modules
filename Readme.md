# Veelgebruikte issues voor aanvullende functies

## Doel
Dit project bevat veelvoorkomende issues met betrekking tot taken gerelateerd aan een module of functionaliteit welke niet relevant zijn als standaard issue.

## Werking en structuur
Elke module bestaat uit een specifieke folder. In deze folder is een korte toelichting (in de vorm van een Readme) en een voorbeeld-csv te vinden. Dit CSV kan worden gedownload vanuit Bitbucket en worden geïmporteerd naar Jira.

## Hoe download in een CSV vanuit Bitbucket?

- Navigeer naar het betreffende bestand en klik op "Open raw" (te vinden onder de drie puntjes). Het CSV bestand wordt nu geopend in de browser en kan worden opgeslagen.

## Hoe importeer ik een CSV naar Jira?

- Ga in Bitbucket naar de correcte folder, open het correcte bestand en kopieer de informatie naar een eigen CSV bestand
- Open Jira en klik op "Issues" > "Import from CSV"
- Volg de stappen en voeg de issues toe aan het correct document

## Opmaak van een CSV
Een import-csv gebruikt te volgende opmaak:

    Summary, Description
    "Voorbereiding databamigratie","Om de migratie voor te bereiden dient het volgende te worden uitgevoerd:
    - Installatie Ubertheme migration tool
    - Installatie ondersteunende modules"

De waarden zijn dus puntkomma gescheiden. Door een enter te gebruiken kun je een item op een nieuwe regel laten beginnen.

## Toelichting keuze voor Bitbucket

* Geen overbodige / irrelevante issues in het standaardproject zodat je altijd het standaar project kunt clonen
* Alle overige issues centraal gedocumenteerd, maar gestructureerd per onderwerp (dus een map voor de Connector, map voor ERP koppeling, et cetera).
* Opslaan als CSV die dan eenvoudig kan worden gedownload vanaf Bitbucket en geïmporteerd in Jira
* Vanaf de kennisbank / Merlin hoef je geen bijlagen te beheren maar enkel te linken naar Bitbucket
* Mogelijkheid voor een handleiding / toelichting in Bitbucket (Readme)
* Eenvoudig downloaden vanuit de Bitbucket interface (direct downloaden van de CSV)
* Versiebeheer met naam van degene die de wijziging heeft gemaakt (kan online in Bitbucket).

## Overige vragen

### Hoe bewerk ik de lijst met standaard-issues?
Dit kan je online in Bitbucket doen.

### Hoe voeg ik een nieuw bestand toe?
Dit kun je online via Bitbucket doen.

### Hoe voeg ik een nieuwe map toe?
Dit is helaas het enige onderdeel dat niet online kan worden toegevoegd, dus dit moet je via de terminal doen.